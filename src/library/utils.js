import { BASEURL, REQUEST_METHOD, RESPONSE_CODE, RESPONSE_TYPE, DEFAULT_DATA, TIME_ZONE } from './Constants';
import { LANGUAGE } from '../language/index';
import { ITEM_PER_PAGE_LIST } from './Constants';

export function getRequestHeader(accessToken) {
    let headers = {
        "Content-Type": "application/json",
        "accept": "application/json"
    };
    if (accessToken !== null) {
        headers["Authorization"] = `Token ${accessToken}`;
    }
    return headers;
}

export const fetchApi = (endPoint, accessToken, method, successCallBack, errorCallBack, body = null, setBaseUrl = true, responseType = RESPONSE_TYPE.JSON) => {
    let options = {
        method: method,
        headers: getRequestHeader(accessToken)
    };

    let requestUrl = endPoint;
    if (setBaseUrl) {
        requestUrl = `${BASEURL}${endPoint}`;
    }

    if (method === REQUEST_METHOD.GET && body !== null) {
        errorCallBack("GET request does not support body")
        return null
    } else if (method !== REQUEST_METHOD.GET) {
        options["body"] = JSON.stringify(body)
    }
    fetch(requestUrl, options)
        .then(response => {
            if (response.status >= 400) {
                return response
            } else {
                switch (responseType) {
                    case RESPONSE_TYPE.JSON:
                        return response.json();
                    case RESPONSE_TYPE.BLOB:
                        return response.blob();
                    case RESPONSE_TYPE.NULL:
                        return DEFAULT_DATA
                }
            }
        })
        .then(responseJson => {
            if (responseJson.type === 'cors') {
                apiErrorHandler(responseJson, errorCallBack)
            } else {
                successCallBack(responseJson)
            }
        }).catch(error => {
            errorCallBack(`Something Went Wrong. error : ${error}`)
        })
}

export function apiErrorHandler(response, errorCallBack) {
    switch (response.status) {
        case RESPONSE_CODE.INTERNAL_SERVER_ERROR:
            return response.statusText;
        default:
            getApiErrorMessage(response, errorCallBack);
            break;
    }
}

function getApiErrorMessage(response, errorCallBack) {
    (response.json()).then((data) => {
        if (Array.isArray(data))
            return errorCallBack(data)
        let key = Object.keys(data)
        key = key[0]
        if (typeof data[key] === 'string')
            return errorCallBack(data[key])
        if (data.length > 0) {
            data = data[0]
        }
        let keys = Object.keys(data)
        if (keys.length > 0) {
            keys = keys[0]
        }
        return errorCallBack(data[keys][0])
    })
}

export async function ConvertPageImageToBlob(accessToken, imageUrl) {
    let requestOptions = {
        method: REQUEST_METHOD.GET,
        headers: getRequestHeader(accessToken),
    };
    let response = await fetch(imageUrl, requestOptions)
    response = await response.blob()
    let blobResponse = await URL.createObjectURL(response);
    return blobResponse;
}

export function setUserDataToStorage(data) {
    localStorage.setItem('userData', JSON.stringify(data))
}

export function getUserDataFromStorage(data) {
    try {
        const serializedState = localStorage.getItem('userData')
        if (serializedState === null) return undefined
        return JSON.parse(serializedState)
    } catch (e) {
        return undefined
    }
}

export function deleteUserDataFromStorage() {
    localStorage.removeItem('userData');
}

export function csvToJSON(csv) {
    csv = csv.replace('\r', '');
    var lines = csv.split("\n");
    var result = [];
    var headers = lines[0].split(",");
    for (var i = 1; i < lines.length; i++) {
        var obj = {};
        var currentline = lines[i].split(",");
        for (var j = 0; j < headers.length; j++) {
            if (currentline[j] !== undefined && currentline[j] !== "") {
                obj[headers[j]] = currentline[j];
            }
        }
        if (Object.keys(obj).length > 0) {
            result.push(obj);
        }
    }
    return result;
}

export function readFile(file) {
    return new Promise((resolve, reject) => {
        let fr = new FileReader();
        fr.onload = x => resolve(fr.result);
        fr.readAsText(file);
    })
}

export function setTimezone(date, timeZone = TIME_ZONE[0].value) {
    var invdate = new Date(date.toLocaleString('en-US', {
        timeZone: timeZone
    })).toString();
    invdate = invdate.slice(0, invdate.indexOf('G'))
    return invdate;
}

export function getLanguageObject(lang) {
    for (let i = 0; i < LANGUAGE.length; i++) {
        if (lang === LANGUAGE[i].id) {
            return LANGUAGE[i].value
        }
    }
}

export function parseLanguage(navigation, language) {
    if (language !== undefined && language !== []) {
        navigation.map(value => {
            if (value._children) {
                value._children.map(data => {
                    let temp = data.id
                    data.name = language[temp]
                })
            }
            if (value.id) {
                value.name = language[value.id]
            }
        })
        return navigation
    }
}

export function removeArrayElement(array: array, item: String) {
    var index = array.indexOf(item);
    if (index > -1) {
        array.splice(index, 1);
    }
    return array;
}

export function convertArrayToString(array: array, separator: string) {
    let string = ''
    if (array.length >= 1)
        array.map(value => {
            return (string = string + value + separator)
        })
    string = string.slice(0, string.length - 1)
    return string
}

export function convertStringToArray(data: String, separator: String) {
    let option = []
    let subString = ''
    for (let i = 0; i < data.length; i++) {
        if (data[i] === separator) {
            i++
            option.push(subString)
            subString = ''
        }
        subString = subString + data[i]
    }
    option.push(subString)
    return option

}

export function bindMultiplePropertyToOneField(array: array, propertiesToBind: array, propertyKey: String) {
    array.map(value => {
        value[propertyKey] = {}
        for (var property in value) {
            if (propertiesToBind.includes(property)) {
                value[propertyKey][property] = value[property]
                delete value[property]
            }
        }

    })
    return array
}

export function updateStringBySeparator(str: String, sourceSeparator: String, targetSeparator: String) {
    let array = convertStringToArray(str, sourceSeparator)
    let string = convertArrayToString(array, targetSeparator)
    return string

}

export function getPageNumberByItemCount(itemCount, currentPageNumber) {
    if (currentPageNumber !== 1 && itemCount === (currentPageNumber - 1) * ITEM_PER_PAGE_LIST + 1) {
        return currentPageNumber - 1
    }
    else {
        return currentPageNumber
    }
}

export function getRandomString(startingThreeLetters) {
    const dateObj = new Date();
    const month = dateObj.getMonth() + 1;
    const day = String(dateObj.getDate()).padStart(2, '0');
    const year = dateObj.getFullYear();
    const output = year + '/' + month + '/' + day
    return startingThreeLetters + output + Math.floor(Math.random() * 100)
}
